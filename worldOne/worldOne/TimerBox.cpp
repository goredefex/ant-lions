#include "TimerBox.h"


//Constructs ----------------------------------------

//Default Constructor
TimerBox::TimerBox(void) {
	
	this->setStart(this->turnToSecs(clock()));

}

//Destructor - Void
TimerBox::~TimerBox(void) {

}




//Getters ----------------------------------------

clock_t TimerBox::getStart() {
	return this->start;

} //end function

clock_t TimerBox::getEnd() {
	return this->end;

} //end function

double TimerBox::getTimePassed() {
	return this->timePassed;

} //end function




//Setters ----------------------------------------

void TimerBox::setStart(clock_t newTime) {
	this->start = newTime;

} //end function

void TimerBox::setEnd(clock_t newTime) {
	this->end = newTime;

} //end function

void TimerBox::setTimePassed(double newTime) {
	this->timePassed = newTime;

} //end function





//Functions ----------------------------------------

clock_t TimerBox::turnToSecs(clock_t currTime) {
	return currTime/CLOCKS_PER_SEC;

} //end function

void TimerBox::checkTimePassed() {
	this->setTimePassed(difftime(this->getEnd(), this->getStart()));

} //end function

void TimerBox::captureCurrEnd() {
	this->setEnd(this->turnToSecs(clock()));

} //end function

void TimerBox::captureCurrStart() {
	this->setStart(this->turnToSecs(clock()));

} //end function
