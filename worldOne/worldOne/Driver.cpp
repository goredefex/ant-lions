#ifndef DRIVER_H
#define DRIVER_H

#include <iostream>
#include <string>
#include "TimerBox.h"
#include "Output.h"
#include "World.h"
#include "Organism.h"
#include "Lion.h"
#include "Ant.h"

using namespace std;


int main() {

	World world;
	world.setupWorld();
	
	TimerBox timer;
	Output GUI;
	int counter = 0;
	bool gameRunning = false;

	//Enter Game Loop
	do {
		
		//Begin Game
		gameRunning = true;

		//Flag Extinction Event
		if(world.countAnts()==0 || world.countLions()==0) {
			gameRunning = false;
		
		//Enter Turn Loop
		} else {

			timer.captureCurrEnd();
			timer.checkTimePassed();
		
			//Find out if 1 second has passed
			if(timer.getTimePassed() >= 0.0) {

				//World/Animal Key Events
				world.famine();
				world.breedLions();
				world.breedAnts();
				
				//Display Results
				GUI.wipeScreen();
				GUI.lineBreak();
				GUI.displayALine(GUI.convertInt(timer.getTimePassed())+" :Second Has Passed - "+GUI.convertInt(counter)+" :Iterations Completed");
				GUI.displayALine("Ants: "+GUI.convertInt(world.countAnts())+" - Lions: "+GUI.convertInt(world.countLions()));
				cout << world;

				//Reset Timer
				timer.captureCurrStart();
				counter++;

				//Take Game Turn
				world.takeTurn();

			} 
		}

	} while(gameRunning); //end game loop
	
	cout << "Game Over Species Extinct!" << endl;
	system("pause");

	return 0;

} //end entry point

#endif