#include "Output.h"


//Constructs ----------------------------------------

//Default Constructor
Output::Output(void){


}


//Destructor
Output::~Output(void){ 
	
}



//Functions ----------------------------------------

void Output::lineBreak() {
	cout << endl;

} //end function

void Output::displayALine(string newLine) {
	cout << newLine << endl;

} //end function

void Output::displayText(string newLine) {
	cout << newLine;

} //end function

void Output::wipeScreen() {
	system("CLS");

} //end function

string Output::convertInt(int number) {
   stringstream conversion;
   conversion << number;
   
   return conversion.str();
}


//Overloads --------------------------------------------
std::ostream& operator<<( ostream& inOrg, Organism& organism ) {
	
	if(organism.whoIsThis()=="ant") {
		//system("Color 1A");
		cout << "|A|";

	} else if(organism.whoIsThis()=="lion") {
		//system("Color 3C");
		cout << "|L|";

	} 

	return inOrg;

} //end overload

std::ostream& operator<<( ostream& output, World& world ) {
	
	for(int row=0; row<world.getGridHeight(); row++) {
		cout << "||";

		for(int column=0; column<world.getGridWidth(); column++) {

			//Safety Test
			Organism* test = &world.getLandscapePosition(row, column);

			//Check Land
			if(test != NULL) {
				cout << world.getLandscapePosition(row, column);

			} else {
				cout << "| |";
			}

		} //end inner

		cout << "||" << endl;

	} //end outer

	return output;

} //end overload