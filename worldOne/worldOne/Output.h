#ifndef OUTPUT_H
#define OUTPUT_H

#include <iostream>
#include <sstream>
#include <string>
#include "World.h"
#include "Organism.h"

using namespace std;

class Output {

	//Private Member Vars
	private:


	//Public Methods
	public:
		Output(void);
		~Output(void);

		//Functions
		void lineBreak();
		void displayALine(string newLine);
		void displayText(string newLine);
		void wipeScreen();
		string convertInt(int number);

		friend ostream& operator<<( ostream &output, Organism& organism );
		friend ostream& operator<<( ostream &output, World &world );

};

#endif