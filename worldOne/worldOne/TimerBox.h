#ifndef TIMERBOX_H
#define TIMERBOX_H

#include <time.h>
using namespace std;

class TimerBox {

	//Private Member Vars
	private:
		clock_t start, end;
		double timePassed;

	//Public Methods
	public:
		//Constructs
		TimerBox(void);
		~TimerBox(void);

		//Getters
		clock_t getStart();
		clock_t getEnd();
		double getTimePassed();

		//Setters
		void setStart(clock_t newTime);
		void setEnd(clock_t newTime);
		void setTimePassed(double newTime);

		//Functions
		clock_t turnToSecs(clock_t currTime);
		void checkTimePassed();
		void captureCurrEnd();
		void captureCurrStart();

};

#endif